import { FileOutlined, PieChartOutlined, UserOutlined } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, theme,Image } from 'antd';
import React, { useState } from 'react';
import {useNavigate} from 'react-router-dom';
import './App.css'
// import HomePage from '../page/home/HomePage';
// import Customer from '../page/customer/CustomerPage';


const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
  getItem('Option 1', '1', <PieChartOutlined />),
  // getItem('Option 2', '2', <DesktopOutlined />),
  getItem('User', 'sub1', <UserOutlined />, [
    getItem('Customer', '/customer', <UserOutlined />,),
    getItem('Bill', '4'),
    getItem('User', '5'),
  ]),
  // getItem('Team', 'sub2', <TeamOutlined />, [getItem('Team 1', '6'), getItem('Team 2', '8')]),
  getItem('Files', '9', <FileOutlined />),
];
const AppLayout = ({children}) => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();
  const {
    token: { colorBgContainer
     },
  } = theme.useToken();
  return (
    <Layout
      style={{
        minHeight: '120vh',
      }}
    >
      <Sider style={{ background: "linear-gradient(180deg, rgba(255, 208, 39, 0.6) 6.1%, rgba(254, 209, 46, 0) 186.23%)"}}  collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
        <div
          style={{
            height: 54,
            margin: 0,
            padding: 10,
            color: 'white',
            background: "#D9AA1C"
  
          }}
        >
        <Image width={40} src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" />
         <b style={{paddingLeft:3,fontSize:20}}>  DailyShare </b>
        </div>
        <Menu 
        onSelect={(e)=>{
          navigate(e.key)
          console.log(items.find((i)=> items))
    
        }} s style={{ background:"unset"}} defaultSelectedKeys={['1']}  mode="inline" items={items}  />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            backgroundColor:"white"
          }}
        />
        <Content
          style={{
            margin: '0 16px',
          }}
        >
          <Breadcrumb
            style={{
              margin: '16px 0',
            }}
          >
            <Breadcrumb.Item>User</Breadcrumb.Item>
            <Breadcrumb.Item>Bill</Breadcrumb.Item>
          </Breadcrumb>
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
            }}
          >
         {children}
          </div>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
            background:"unset"
          }}
        >
          Ant Design ©2023 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};
export default AppLayout;