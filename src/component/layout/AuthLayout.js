import React from "react";
const AuthLayout = (props) => {
    return (
        <div className='container-fluid m-auto '>
            <div className='row  bg-warning form-login'>
                <div className='col-md-12 col-lg-4 pt-5  '>
                    <div className='text-center text-uppercase pt-5'>
                        <br/>
                        <br/>
                        <img  width={100} src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" />
                    </div>
                    <h2 className='text-center text-uppercase text-white'> Daily Share </h2>

                </div>
                <div className='col-md-12 col-lg-8  bg-light pt-5'>
                    {props.children}
                </div>
            </div>
        </div>

    )
};
export default AuthLayout;
