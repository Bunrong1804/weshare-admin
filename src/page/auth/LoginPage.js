import React, {useState} from 'react'
import {request} from '../../util/api'
import {Button, Checkbox, Form, Input, message, Image, Space} from 'antd';
import { Link } from 'react-router-dom'
import './login.css'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function LoginPage() {

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const handleLogin = () => {
        var params = {
            "email": username,
            "password": password
        }
        request("post", "/auth/login", params).then(res => {
            if (res.data.data.error === true) {
                message.warning(res.data.data.message);
                localStorage.setItem("is_login", "0")
            } else {
                localStorage.setItem("is_login", "1") // is_login = 1
                localStorage.setItem("profile", JSON.stringify(res.data.data.user))
                window.location.href = "/"
            }

            // if(res.data && res.data.is_login){
            //    // message.success("Login success")
            //    localStorage.setItem("is_login","1") // is_login = 1
            //    localStorage.setItem("profile",JSON.stringify(res.data.profile))
            //    window.location.href = "/"
            // }else{
            //     console.log(`error ${JSON.stringify(res.data)}`)
            //    message.warning(res)
            // }
        })
    }

    return (
        <div className='p-5 center col-11 m-auto bg-light center card border-0'>
            <div className='m-auto pt-5 '>

                <h1 className='center text-uppercase'> Login </h1>
            </div>
            <Form
                name="basic"
                initialValues={{
                    remember: true,
                }}
                onFinish={handleLogin}
                // onFinishFailed={handleLogin}
                autoComplete="off"
            >
                <Form.Item
                    label="Username"
                    name="username"
                    onChange={(event) => {
                        setUsername(event.target.value)
                    }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    onChange={(event) => {
                        setPassword(event.target.value)
                    }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item
                    name="remember"
                    valuePropName="checked"
                    wrapperCol={{
                        offset: 3,
                        span: 10,
                    }}
                >
                    <Space>
                        <Link to={"/register"}>Register</Link>
                        <Checkbox>Remember me</Checkbox>
                    </Space>
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 19,
                        span: 10,
                    }}
                >
                    <Button type="primary" htmlType="submit" className='bg-warning px-5 '>
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>

    )
}
