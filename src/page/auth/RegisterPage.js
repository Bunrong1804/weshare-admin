import React, {useState} from 'react'
import {
    Button, Checkbox,
    Form,
    Input, message,
    Space,
  Progress

} from "antd";
import {Link} from "react-router-dom";
import {request} from "../../util/api";

export default function RegisterPage() {
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const handleRegister = () => {

        var params = {
            "email": email,
            "password": password,
            "name": username,
        }
        request("post", "/auth/register", params).then(res => {
            if (res.data?.error) {
                message.warning(JSON.stringify(res.data?.message));
            }else{
                message.success("Register success");
                setTimeout(() => {
                    window.location.href = "/login"
                }, 500);

            }
            // if (res.data.data.error === true) {
            //     message.warning(res.data.data.message);
            // } else {
            //     localStorage.setItem("is_login", "1") // is_login = 1
            //     localStorage.setItem("profile", JSON.stringify(res.data.data.user))
            // }

            // if(res.data && res.data.is_login){
            //    // message.success("Login success")
            //    localStorage.setItem("is_login","1") // is_login = 1
            //    localStorage.setItem("profile",JSON.stringify(res.data.profile))
            //    window.location.href = "/"
            // }else{
            //     console.log(`error ${JSON.stringify(res.data)}`)
            //    message.warning(res)
            // }
        })
    }
  return (
      <div className='p-5 center col-11 m-auto bg-light center card border-0'>
          <div className='m-auto pt-5 '>

              <h1 className='center text-uppercase'> Register </h1>
          </div>
          <Form
              name="basic"
              initialValues={{
                  remember: true,
              }}
              onFinish={handleRegister}
              // onFinishFailed={handleLogin}
              autoComplete="off"
              labelAlign='left'
          >
              <Form.Item
                  label="Username"
                  name="username"
                  onChange={(event) => {
                      setUsername(event.target.value)
                  }}
                  rules={[
                      {
                          required: true,
                          message: 'Please input your username!',
                      },
                  ]}
              >
                  <Input/>
              </Form.Item>

              <Form.Item
                  label="Email"
                  name="email"
                  onChange={(event) => {
                      setEmail(event.target.value)
                  }}
                  rules={[
                      {
                          required: true,
                          message: 'Please input your Email!',
                      },
                  ]}
              >
                  <Input/>
              </Form.Item>
              <Form.Item
                  label="Password"
                  name="password"
                  onChange={(event) => {
                      setPassword(event.target.value)
                  }}
                  rules={[
                      {
                          required: true,
                          message: 'Please input your password!',
                      },
                  ]}
              >
                  <Input.Password/>
              </Form.Item>

              <Form.Item
                  name="remember"
                  valuePropName="checked"
                  wrapperCol={{
                      offset: 3,
                      span: 10,
                  }}
              >
                  <Space>
                      <Link to={"/login"}>Back To Login</Link>
                  </Space>
              </Form.Item>

              <Form.Item
                  wrapperCol={{
                      offset: 19,
                      span: 10,
                  }}
              >
                  <Button type="primary" htmlType="submit" className='bg-warning px-5 '>
                      Submit
                  </Button>
              </Form.Item>
          </Form>
      </div>

  )
}
