import React, { useState } from 'react';
import { Form, Input, Button } from 'antd';
import './login.css'
import 'bootstrap'
const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        // Handle form submission logic here
    };

    return (
        <div className="container">
            <Form onFinish={handleSubmit}>
                <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Please enter your email!' }]}>
                    <Input value={email} onChange={(e) => setEmail(e.target.value)} />
                </Form.Item>
                <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Please enter your password!' }]}>
                    <Input.Password value={password} onChange={(e) => setPassword(e.target.value)} />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">Log In</Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default LoginForm;
