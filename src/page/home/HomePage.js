import React from 'react';
import { Button, Checkbox, Form, Input } from 'antd';
import {useNavigate} from "react-router-dom"
import "./HomePage.css"

const HomeScreen  = () => {

  const navigate = useNavigate()

  const onClick = () => {
    navigate("/product")
  }
  const profile = JSON.parse(localStorage.getItem("profile"))
  console.log(profile)
  return (
    <div>
      <div className={"home"} onClick={onClick}>Click me</div>
      <button
        onClick={()=>{
          localStorage.setItem("is_login","0")
          window.location.href = "/login"
        }}
      >Logout</button>
      {[3,4,5,6,6,6,6,4,5,5,5].map((item,index)=>(
            <h1 key={index} style={{height:80}}>{item}{profile.name}</h1>
      ))}
    </div>
  )

};

export default HomeScreen;
